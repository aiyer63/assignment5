#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

# -----------
# imports
# -----------
from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'},
         {'title': 'Algorithm Design', 'id': '2'},
         {'title': 'Python', 'id': '3'}]


@app.route('/book/JSON/')
def bookJSON():
    return render_template("JSON.html", books=books)


@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)


@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    return render_template('newBook.html')


@app.route('/book/<int:book_id>/edit/', methods=['GET', 'POST'])
def editBook(book_id):
    book_title = "something went wrong"
    for dic in books:
        if dic['id'] == str(book_id):
            book_title = dic['title']              
    return render_template('editBook.html', id = book_id, title = book_title)


@app.route('/book/<int:book_id>/delete/', methods=['GET', 'POST'])
def deleteBook(book_id):
    book_title = "something went wrong"
    for dic in books:
        if dic['id'] == str(book_id):
            book_title = dic['title']
    return render_template('deleteBook.html', id = book_id, title = book_title)

@app.route('/avi_is_cool/<book>',  methods=['GET', 'POST'])
def dictEdit(book):
    for dic in books:
            if dic['id'] == book:
                new_name = request.form['New Title']
                dic['title'] = new_name
                return redirect(url_for('showBook'))
    return redirect(url_for('showBook'))     

@app.route('/delDict/<book>',  methods=['GET', 'POST'])
def dictDel(book):
    for dic in books:
            if dic['id'] == book:
                books.remove(dic)
                return redirect(url_for('showBook'))
    return redirect(url_for('showBook'))     

@app.route('/newDict/',  methods=['GET', 'POST'])
def dictAdd():
    book_title = request.form['NewBook']
    ids = []
    for dic in books:
        ids.append(int(dic['id']))
    ids.sort()
    prev = 0
    for num in ids:
        if not prev:
            prev = num
        else:
            if num - prev > 1:
                #Gap in IDs
                books.append({'title': book_title, 'id': (prev + 1)})
                break
            prev = num
    else:
        #no gap
        books.append({'title': book_title, 'id': str((ids[-1] + 1))})   
        print(books)  
    return redirect(url_for('showBook'))    

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
